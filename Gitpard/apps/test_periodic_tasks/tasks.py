# -*- coding: utf-8 -*-

from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from datetime import datetime
from Gitpard.apps.test_periodic_tasks.models import TaskHistory

logger = get_task_logger(__name__)
 
@periodic_task(run_every=(crontab(hour="*", minute="*", day_of_week="*")), 
    ignore_result=True)
def scraper_example():
    logger.info("Start task")
    now = datetime.now()
    date_now = now.strftime("%d-%m-%Y %H:%M:%S")
    # Perform all the operations you want here
    result = 2+2
    # The name of the Task, use to find the correct TaskHistory object
    name = "scraper_example"
    taskhistory = TaskHistory.objects.get_or_create(name=name)[0]
    taskhistory.history.update({date_now: result})
    taskhistory.save()
    logger.info("Task finished: result = %i" % result)
