from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

# Indicate celery to use the default Django settings module
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Gitpard.settings')

app = Celery('Gitpard')
app.config_from_object('django.conf:settings')
# This line will tell celery to autodiscover all your task.py that are in your app folders
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
