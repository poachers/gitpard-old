# Начальные настройки проекта при клонировании

##Разработчикам
#### Устанавливаем virtualenv и вводдим в корне проекта:

```
#!bash

$ virtualenv devenv
```

####После чего добавляем в файл `devenv/bin/activate`:

```
#!python

DJANGO_SETTINGS_MODULE="Gitpard.development_settings"
export DJANGO_SETTINGS_MODULE
```

####Активируем рабочее окружение:

```
#!bash

$ source devenv/bin/activate
```
####Скачиваем зависимости:

```
#!bash

$ pip install -r requirements/dev.txt
```

### Установка и настройка *celery* и *rabbitmq*

```
#!bash

$ sudo apt-get install rabbitmq-server
```

####Открываем file procfile любым текствым редактором
#####Например:

```
#!bash

$ nano ~/.procfile
```
####И добавляем туда следующую строчку:

```
#!bash

export PATH=$PATH:/usr/local/sbin
```

####Затем:

```
#!bash

sudo rabbitmqctl add_user myuser mypassword
sudo rabbitmqctl add_vhost myvhost
sudo rabbitmqctl set_permissions -p myvhost myuser ".*" ".*" ".*"
```

#### После чего нужно будут синхронизировать базу данных и осуществить миграцию:

```
#!bash

python manage.py migrate djcelery
python manage.py syncdb
```

### Запуск проекта:
#### В раздельных сессиях терминала:

```
#!bash

$ python manage.py celeryd -B -l info
```
```
#!bash

$ python manage.py runserver 0.0.0.0:8000
```





====================================================================

### Устанавливаем virtualenv и вводдим в корне проекта:
#### окружение разработчикам
`virtualenv devenv`
#### окружение тестировщиков
testenv

    После чего добавляем в файл `devenv/bin/activate`:

## Разработчикам
    devenv/bin/activate:
        ```python
        DJANGO_SETTINGS_MODULE="Gitpard.development_settings"
        export DJANGO_SETTINGS_MODULE
        ```
## Тестировщикам
    testenv/bin/activate:
        ```python
        DJANGO_SETTINGS_MODULE="Gitpard.test_settings"
        export DJANGO_SETTINGS_MODULE
        ```

Активируем рабочее окружение:
    `source devenv(or testenv)/bin/activate`
Скачиваем зависимости:
    `pip install -r requirements/dev.txt(or test.txt)`

## Установка и настройка celery и rabbitmq

    `$sudo apt-get install rabbitmq-server`

    Открываем file procfile любым текствым редактором
    Например:
    `vi ~/.procfile` 
    И добавляем туда следующую строчку:
    `export PATH=$PATH:/usr/local/sbin` 

    Затем:
    `sudo rabbitmqctl add_user myuser mypassword`
    `sudo rabbitmqctl add_vhost myvhost`
    `sudo rabbitmqctl set_permissions -p myvhost myuser ".*" ".*" ".*"`

    После чего нужно будут синхронизировать базу данных и осуществить миграцию:
        `python manage.py migrate djcelery`
        `python manage.py syncdb`

### Запуск проекта:
    В раздельных сессиях терминала:
    `python manage.py celeryd -B -l info`
    'python manage.py runserver'

#### Неплохая статья про pip и virtualenv:
    http://www.dabapps.com/blog/introduction-to-pip-and-virtualenv-python